/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.unscramble.ui.list

import android.view.View
import androidx.lifecycle.*
import com.example.android.unscramble.model.Plant
import com.example.android.unscramble.network.NetworkService
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class _ListViewModel2 : ViewModel() {
    private val TAG = this::class.simpleName

    private val _plantList: MutableLiveData<List<Plant>> = MutableLiveData()
    val plantList: LiveData<List<Plant>>
        get() = _plantList

    fun tesRetrofit() {
        viewModelScope.launch {
            val list = NetworkService().allPlants()
            list.let {
                _plantList.value = list
//                progressStaus(View.GONE)
                delay(3000)
            }

        }
    }

    private val _progressState = MutableLiveData(View.GONE)
    val progressState: LiveData<Int>
        get() = _progressState
    fun progressStaus(isShow: Int) {
        _progressState.value = isShow
    }

    init {
        tesRetrofit()
    }
}
