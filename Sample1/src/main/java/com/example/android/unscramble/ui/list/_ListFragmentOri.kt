/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package com.example.android.unscramble.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.*
import com.example.android.unscramble.R
import com.example.android.unscramble.databinding.ListFragmentBinding
import com.example.android.unscramble.model.Plant
import com.example.android.unscramble.ui.game.MAX_NO_OF_WORDS
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * Fragment where the game is played, contains the game logic.
 */
class _ListFragmentOri : Fragment() {
    private val TAG = this::class.simpleName

    private lateinit var binding: ListFragmentBinding

    private val viewModel: ListViewModel by viewModels()
    val mAdapter by lazy { ContractListAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout XML file and return a binding object instance
        binding = DataBindingUtil.inflate(inflater, R.layout.list_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.listViewModel = viewModel
        binding.maxNoOfWords = MAX_NO_OF_WORDS
        binding.lifecycleOwner = viewLifecycleOwner

        binding.netButton.setOnClickListener {
            viewModel.testRetrofit()
        }

        binding.recylerView.adapter = mAdapter
        binding.recylerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recylerView.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
        viewModel.plantList.observe(viewLifecycleOwner) {
            mAdapter.submitList(it)
        }
    }



    class ContractListAdapter: ListAdapter<Plant, ContractListAdapter.ViewHolder>(ListDiffCallBack()) {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val holder = ViewHolder.create(parent)
            return holder
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val current = getItem(position)
            holder.bind(current)
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            private val wordItemView: TextView = itemView.findViewById(R.id.textView)
            private val deleteBtn: Button = itemView.findViewById(R.id.deleteBtn)

            fun bind(dto: Plant?) {
                dto?.let {
//                    val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault()).format(Date(it.updateDate))
                    wordItemView.text = dto.name
                    deleteBtn.setOnClickListener{

                    }
                }
            }

            companion object {
                fun create(parent: ViewGroup): ViewHolder {
                    val view: View = LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_item, parent, false)
                    return ViewHolder(view)
                }
            }
        }



        class ListDiffCallBack: DiffUtil.ItemCallback<Plant>() {
            override fun areItemsTheSame(oldItem: Plant, newItem: Plant): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Plant, newItem: Plant): Boolean {
                val isChangeData = oldItem == newItem
                return isChangeData
            }
        }
    }
}
