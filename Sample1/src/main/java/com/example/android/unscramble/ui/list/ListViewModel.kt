package com.example.android.unscramble.ui.list

import android.view.View
import androidx.lifecycle.*
import com.example.android.unscramble.model.Plant
import com.example.android.unscramble.network.NetworkService
import kotlinx.coroutines.*

class ListViewModel : ViewModel() {
    private val TAG = this::class.simpleName

    private val _plantList: MutableLiveData<List<Plant>> = MutableLiveData()
    val plantList: LiveData<List<Plant>>
        get() = _plantList

    fun testRetrofit() {
        launchDataLoad(viewModelScope) {
            val list = NetworkService().allPlants()
            _plantList.value = list
        }
    }


    private val _progressState = MutableLiveData(View.GONE)
    val progressState: LiveData<Int> = _progressState

    /**
     * Progress 상태 처리
     */
    suspend fun progressStaus(showState: Int) {
        withContext(Dispatchers.Main) {
            _progressState.value = showState
        }
    }

    /**
     * Progress Layout을 visible 처리
     */
    suspend fun showProgress() {
        progressStaus(View.VISIBLE)
    }

    /**
     * Progress Layout을 gone 처리
     */
    suspend fun hideProgress() {
        progressStaus(View.GONE)
    }

    /**
     * 프로그래스 상태 처리 및 오류 알림
     */
    fun launchDataLoad(viewModelScope: CoroutineScope, block: suspend () -> Unit): Job {
        return viewModelScope.launch {
            try {
//                _spinner.value = true
                showProgress()
                block()
            } catch (error: Throwable) {
                error.printStackTrace()
                hideProgress()
//                _snackbar.value = error.message
            } finally {
//                _spinner.value = false
                hideProgress()
            }
        }
    }


    init {
        testRetrofit()
    }
}
