package com.example.android.unscramble

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.android.unscramble.databinding.MainActivityBinding
import com.example.android.unscramble.ui.list.ListViewModel

class MainActivity : AppCompatActivity() {
    private val viewModel: ListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<MainActivityBinding>(this, R.layout.main_activity)
        binding.lifecycleOwner = this
        binding.activityViewModel = viewModel
    }
}