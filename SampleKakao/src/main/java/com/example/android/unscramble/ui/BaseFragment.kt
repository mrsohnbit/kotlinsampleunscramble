/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package com.example.android.unscramble.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.android.unscramble.activity.IActivity
import com.example.android.unscramble.activity.MainActivity
import com.example.android.unscramble.ui.dialog.CustomDialog
import com.example.android.unscramble.ui.list.ActivityViewModel

open class BaseFragment: Fragment(), IActivity {
    private val TAG = this::class.simpleName

//    private lateinit var binding: DetailFragmentBinding
    protected val activityViewModel: ActivityViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // 알림 메시지
        activityViewModel.alertMessage.observe(this) {message ->
            if (!message.isEmpty()) {
                CustomDialog.show(requireContext(), message)
            }
        }
    }

    override fun replaceFragment(fragment: Fragment) {
        if (activity is MainActivity) {
            val baseActivity = activity as MainActivity
            baseActivity.replaceFragment(fragment)
        }
    }
//    override fun onCreateView(
//        inflater: LayoutInflater, container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View {
//        binding = DataBindingUtil.inflate(inflater, R.layout.detail_fragment, container, false)
//        return binding.root
//    }
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//
//        binding.detailViewModel = viewModel
//        binding.maxNoOfWords = MAX_NO_OF_WORDS
//        binding.lifecycleOwner = viewLifecycleOwner
//
//        binding.netButton.setOnClickListener {
//            CoroutineScope(Dispatchers.IO).launch {
//
//            }
//        }
//
//    }
//
//    /**
//     * OnActivityResult 처리
//     */
//    val resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult(), ActivityResultCallback { data ->
//        if (data.resultCode == AppCompatActivity.RESULT_OK) {
//            Log.i(TAG, "data.resultCode RESULT_OK")
//        } else {
//            Log.i(TAG, "data.resultCode RESULT_CANCEL")
//        }
//    })
}
