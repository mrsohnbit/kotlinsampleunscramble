/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.unscramble.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.android.unscramble.R
import com.example.android.unscramble.databinding.SubActivityBinding

class SubActivity: BaseActivity() {//: BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<SubActivityBinding>(this,
            R.layout.sub_activity
        )
        binding.lifecycleOwner = this
        binding.activityViewModel = activityViewModel
        setContentView(binding.root)




    }

    inline fun <reified T : Fragment> FragmentManager.showFragment(tag: String, container: Int, args: Bundle? = null) {
        val clazz = T::class.java
        var fragment = findFragmentByTag(clazz.name)
        if (fragment != null) {
            this.popBackStack(tag, 0)
        } else {
            fragment = (clazz.getConstructor().newInstance() as T).apply {
                args?.let {
                    it.classLoader = javaClass.classLoader
                    arguments = args
                }
            }
            val transaction = this.beginTransaction()
            transaction.add(container, fragment as Fragment, tag)
            transaction.addToBackStack(tag)
            transaction.commit()
        }
    }

    fun replaceFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        val tag = fragment::class.simpleName;
        transaction.replace(R.id.sub_fragment, fragment, tag)
        transaction.addToBackStack(tag)
        try {
            transaction.commit()
        } catch (e: Exception) {
            e.printStackTrace()
            transaction.commitAllowingStateLoss()
        }
    }


    companion object {
        val FRAGMENT_NAME = "FRAGMENT_NAME"
        fun startActivity(context: Context, fragment: Class<Fragment>, bundle: Bundle) {
            val intent = Intent(context, SubActivity::class.java)
            intent.putExtra(FRAGMENT_NAME, fragment.name)
            context.startActivity(intent)
        }
    }



}