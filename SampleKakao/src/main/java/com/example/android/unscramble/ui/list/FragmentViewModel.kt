package com.example.android.unscramble.ui.list

import android.content.Context
import androidx.lifecycle.*
import com.example.android.unscramble.DefaultDataSource
import com.example.android.unscramble.IDataSource
import com.example.android.unscramble.R
import com.example.android.unscramble.model.Documents
import com.example.android.unscramble.model.KakaoImage
import com.example.android.unscramble.model.Meta
import com.example.android.unscramble.network.NetworkService
import kotlinx.coroutines.Dispatchers

class FragmentViewModel(private val dataSource: IDataSource) : ViewModel() {
    private val TAG = this::class.simpleName
    private val context = dataSource.context

    private var meta = Meta(false, 1, 0)

    private fun setMeta(isEnd: Boolean, pageableCount: Int, totalCount: Int) {
        meta = Meta(isEnd, pageableCount, totalCount)
    }

    var paramSort: String = context.resources.getStringArray(R.array.sort_array_value)[0]
    var paramPage: Int = 1

    val list = mutableListOf<Documents>()
    private val _itemList = MutableLiveData<List<Documents>>()
    val itemList: LiveData<List<Documents>> = _itemList

    fun addItem(item: Documents) {
        list.add(item)
        _itemList.value = list
    }

    fun addAll(plantList: List<Documents>) {
        list.addAll(plantList)
        _itemList.value = list
    }

    fun removeItem(item: Documents) {
        list.remove(item)
        _itemList.value = list
    }

    private val _kakaoImage: MutableLiveData<KakaoImage> = MutableLiveData()
    val kakaoImage: LiveData<KakaoImage>
        get() = _kakaoImage


    fun testRetrofit() {
        if (meta.is_end) {
            dataSource.alertMessage("")
            return
        }

        dataSource.launchDataLoad(viewModelScope) {
            val sort = paramSort
            val page = paramPage
            val size = 8
            val query = "아이유"
            val kakaoImage = NetworkService().allPlants(paramSort, paramPage, size, query)
            _kakaoImage.value = kakaoImage
            addAll(kakaoImage.documents)

            meta = kakaoImage.meta
            paramPage += 1
        }
    }

    init {
//        _itemList.value = list
        testRetrofit()
    }



//    val itemList = ListLiveData<Plant>()
//
//    fun addItem(item: Plant) {
//        itemList.add(item)
//    }
//
//    fun addAll(items: List<Plant>) {
//        itemList.addAll(items)
//    }
//
//    fun removeItem(item: Plant) {
//        itemList.remove(item)
//    }

//    private val tempList = mutableListOf<Plant>()
//    private val _plantList: MutableLiveData<List<Plant>> = MutableLiveData()
//    val plantList: LiveData<List<Plant>>
//        get() = _plantList
//
//
//    fun addAllItem(list: List<Plant>) {
//        tempList.addAll(list)
//        _plantList.value = list
//    }
//
//    fun removeItem(position: Int) {
//        tempList.removeAt(position)
//        _plantList.value = tempList
//    }
//
//    fun removeItem(plant: Plant) {
//        Log.i(TAG, "adapter.plantListSize1=${tempList.size}")
//
//        tempList.remove(plant)
//        Log.i(TAG, "adapter.plantListSize2=${tempList.size}")
//        _plantList.value = tempList
//    }



}

/**
 * @activiyModel : Activity에서 사용되고 있는 ViewModel
 *                  progress, alert등 공통 영역을 담당 한다.
 */
class FragmentDataVMFactory(private val context: Context, private val activiyModel: ActivityViewModel) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return FragmentViewModel(DefaultDataSource(context, activiyModel, Dispatchers.IO)) as T
    }
}