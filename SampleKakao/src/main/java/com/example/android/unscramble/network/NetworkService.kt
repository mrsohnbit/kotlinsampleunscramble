package com.example.android.unscramble.network

import com.example.android.unscramble.BuildConfig
import com.example.android.unscramble.model.Documents
import com.example.android.unscramble.model.KakaoImage
import com.example.android.unscramble.model.Plant
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.io.IOException

class NetworkService() {

    val githubUrl = "https://raw.githubusercontent.com/"
    val kakaoUrl = "https://dapi.kakao.com/"

    private val retrofit = Retrofit.Builder()
        .baseUrl(kakaoUrl)
//        .client(OkHttpClient())
        .client(getOkHttpClient())
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val kakaoService = retrofit.create(KakaoService::class.java)
    suspend fun allPlants(
        sort: String, page: Int, size: Int, query: String
    ): KakaoImage = withContext(Dispatchers.Default) {
        val result = kakaoService.getKakaoImage(sort, page, size, query)
//        result.documents.shuffled() // 결과값 반영
        result
    }

//    suspend fun plantsByGrowZone(growZone: GrowZone) = withContext(Dispatchers.Default) {
//        val result = kakaoService.getAllPlants()
//        result.filter { it.growZoneNumber == growZone.number }.shuffled()
//    }

//    suspend fun customPlantSortOrder(): List<String> = withContext(Dispatchers.Default) {
//        val result = kakaoService.getCustomPlantSortOrder()
//        result.map { plant -> plant.plantId }
//    }

    /**
     * OKHttpClient 설정
     */
    fun getOkHttpClient(): OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE)
        if (BuildConfig.DEBUG) {
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        }

        class HeaderInterceptor: Interceptor {
            @Throws(IOException::class)
            override fun intercept(chain: Interceptor.Chain): Response {
                val newRequest = chain.request().newBuilder()
                    .addHeader("Authorization", "KakaoAK ca2b5da1cba84dde2a45f81cafbd84b0")
                    .build()
                return chain.proceed(newRequest)
            }
        }

        val okHttpBuilder = OkHttpClient.Builder()
//        okHttpBuilder.addInterceptor(headerInterceptor)
        okHttpBuilder.addInterceptor(HeaderInterceptor())
        okHttpBuilder.addInterceptor(httpLoggingInterceptor)

        return okHttpBuilder.build()
    }
}




interface KakaoService {
    @GET("v2/search/image")

    suspend fun getKakaoImage(
            @Query("sort") sort: String,    // 결과 문서 정렬 방식, accuracy(정확도순) 또는 recency(최신순), 기본 값 accuracy	| 필수(X)
            @Query("page") page: Int,       // 결과 페이지 번호, 1~15 사이의 값	| 필수(X)
            @Query("size") size: Int,       // 한 페이지에 보여질 문서 수, 1~30 사이의 값, 기본 값 15	| 필수(X)
            @Query("query") query: String   // 검색을 원하는 질의어	| 필수(O)
        ): KakaoImage


    @GET("googlecodelabs/kotlin-coroutines/master/advanced-coroutines-codelab/sunflower/src/main/assets/custom_plant_sort_order.json")
    suspend fun getCustomPlantSortOrder(): List<Documents>
}


interface SunflowerService {
    @GET("googlecodelabs/kotlin-coroutines/master/advanced-coroutines-codelab/sunflower/src/main/assets/plants.json")
    suspend fun getAllPlants(): List<Plant>

    @GET("googlecodelabs/kotlin-coroutines/master/advanced-coroutines-codelab/sunflower/src/main/assets/custom_plant_sort_order.json")
    suspend fun getCustomPlantSortOrder(): List<Plant>
}
