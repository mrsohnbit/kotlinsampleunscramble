/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package com.example.android.unscramble.ui.list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.*
import com.example.android.unscramble.R
import com.example.android.unscramble.databinding.ListFragmentBinding
import com.example.android.unscramble.databinding.RecyclerviewItemBinding
import com.example.android.unscramble.model.Documents
import com.example.android.unscramble.ui.BaseFragment
import com.example.android.unscramble.ui.game.MAX_NO_OF_WORDS

class ListFragment : BaseFragment() {
    private val TAG = this::class.simpleName

    private lateinit var binding: ListFragmentBinding
    private val viewModel: FragmentViewModel by viewModels{ FragmentDataVMFactory(requireContext(), activityViewModel) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.list_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.listViewModel = viewModel
        binding.maxNoOfWords = MAX_NO_OF_WORDS
        binding.lifecycleOwner = viewLifecycleOwner

        val adapter = RecyclerListAdapter(this, viewModel)
        binding.recylerView.adapter = adapter
        binding.recylerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recylerView.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
//        subscribeUi(adapter)

        var beforeItemSize = 0
        viewModel.itemList.observe(viewLifecycleOwner, Observer {
            it?.let {
                Log.i(TAG, "adapterCount.size=${it.size}")
                Log.i(TAG, "adapterCount.itemCount=${adapter.itemCount}")

                adapter.submitList(it)
                beforeItemSize = if (beforeItemSize < it.size) {
                                        // 기존 데이터 갯수보다 많아질 경우 추가 된 영역만 notify
                                        adapter.notifyItemRangeChanged(beforeItemSize, it.size)
                                        it.size
                                    } else {
                                        it.size
                                    }
            }
        })

        binding.recylerView.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                super.onScrolled(recyclerView, dx, dy)
                if (!recyclerView.canScrollVertically(1)) {
                    viewModel.testRetrofit()
                }
            }
        })

        binding.spinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                viewModel.paramSort = resources.getStringArray(R.array.sort_array_value)[position]
            }
        }
    }

    /**
     * OnActivityResult 처리
     */
    val resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult(), ActivityResultCallback { data ->
        if (data.resultCode == AppCompatActivity.RESULT_OK) {
            Log.i(TAG, "data.resultCode RESULT_OK")
        } else {
            Log.i(TAG, "data.resultCode RESULT_CANCEL")
        }
    })

    class RecyclerListAdapter(val fragment: ListFragment, val mViewModel: FragmentViewModel): ListAdapter<Documents, RecyclerListAdapter.RecyclerViewHolder>(ListDiffCallBack()) {
        private val TAG = this::class.simpleName

        inner class RecyclerViewHolder(private val binding: RecyclerviewItemBinding) :
            RecyclerView.ViewHolder(binding.root) {
            fun bind(document: Documents) {
                val context = binding.root.context

                binding.document = document
                binding.deleteBtn.setOnClickListener {
                    binding.document?.let {
                        mViewModel.removeItem(it)
                        notifyItemRemoved(adapterPosition)
                    }
                }
                binding.root.setOnClickListener {
//                    val intent = Intent(context, SubActivity::class.java)
//                    resultLauncher.launch(intent)
                    var detailFragment = DetailFragment()
                    detailFragment.arguments = bundleOf("document" to document)
                    fragment.replaceFragment(detailFragment)

                }
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding: RecyclerviewItemBinding =
                DataBindingUtil.inflate(layoutInflater, R.layout.recyclerview_item, parent, false)
            return RecyclerViewHolder(binding)
        }

        override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
            holder.bind(getItem(position))
        }

    }

    /**
     * ListAdapter의 데이터의 변경 부분만 갱신
     */
    class ListDiffCallBack: DiffUtil.ItemCallback<Documents>() {
        override fun areItemsTheSame(oldItem: Documents, newItem: Documents): Boolean {
            val isChange = oldItem == newItem
            return isChange
        }

        override fun areContentsTheSame(oldItem: Documents, newItem: Documents): Boolean {
            val isChange = oldItem == newItem
            return isChange
        }
    }

}
