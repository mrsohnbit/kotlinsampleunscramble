package com.example.android.unscramble.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class KakaoImageLiveData<T> : MutableLiveData<KakaoImage>() {

    private lateinit var mKakaoImage: KakaoImage
    private lateinit var mMeta: Meta
    val itemList = ListLiveData<Documents>()

    private val _receiveKakaoImageData: MutableLiveData<KakaoImage> = MutableLiveData()
    val receiveKakaoImageData: LiveData<KakaoImage>
        get() = _receiveKakaoImageData

    fun setImageData(kakaoImage: KakaoImage) {
        mKakaoImage = kakaoImage
        addAll(kakaoImage.documents)
        setMeta(kakaoImage.meta)
        _receiveKakaoImageData.value = mKakaoImage
    }

    fun addAll(items: List<Documents>) {
        itemList.addAll(items)
    }

    fun removeItem(items: Documents) {
        itemList.remove(items)
    }

    fun clear() {
        itemList.clear(true)
    }

    fun setMeta(meta: Meta) {
        mMeta = meta
    }


    fun setMeta(isEnd: Boolean, pageableCount: Int, totalCount: Int) {
        mMeta = Meta(isEnd, pageableCount, totalCount)
    }


//    val itemList = ListLiveData<Documents>()
////    init {
////        val kakaoImage = KakaoImage(emptyList(), Meta(false, 0, 0))
////        value = kakaoImage
////        itemList.value = kakaoImage.documents
////    }
//
//
//    fun removeItem(item: Documents) {
//        itemList.remove(item)
//    }
//
//    fun add(item: Documents) {
//        val items: KakaoImage? = value
//        itemList.add(item)
//
//        value = items
//    }
//
//    fun addAll(items: List<Documents>) {
//        val item: KakaoImage? = value
//        itemList.addAll(items)
//        value = items
//    }

//    fun clear(notify: Boolean) {
//        val items: ArrayList<T>? = value
//        items!!.clear()
//        if (notify) {
//            value = items
//        }
//    }
//
//    fun remove(item: Documents) {
//        val items: ArrayList<Documents>? = value
//        items!!.remove(item)
//        value.documents = items
//    }
//}

}