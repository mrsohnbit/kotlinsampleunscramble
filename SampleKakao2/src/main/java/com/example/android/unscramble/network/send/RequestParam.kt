package com.example.android.unscramble.model.network

import androidx.annotation.NonNull

/**
 * API 요청 Parameter Map형태로 반환해준다.
 *
 * @param query 검색을 원하는 질의어	| 필수(O)
 * @param page  결과 페이지 번호, 1~15 사이의 값	| 필수(X)
 * @param sort  결과 문서 정렬 방식, accuracy(정확도순) 또는 recency(최신순), 기본 값 accuracy	| 필수(X)
 * @param size  한 페이지에 보여질 문서 수, 1~30 사이의 값, 기본 값 15	| 필수(X)
 */
class RequestImageParam(@NonNull query: String, page: Int = 1, sort: String = "accuracy", size: Int = 50) {
    val paramMap : Map<String, String> = mapOf("query" to query,
                        "page" to page.toString(),
                        "sort" to sort,
                        "size" to size.toString())
}

