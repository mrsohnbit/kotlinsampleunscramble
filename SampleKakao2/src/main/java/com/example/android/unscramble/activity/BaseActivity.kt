/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.unscramble.activity

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.android.unscramble.R
import com.example.android.unscramble.databinding.ActivityBaseBinding
import com.example.android.unscramble.ui.list.ActivityViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

open class BaseActivity : AppCompatActivity() {
    protected val TAG = this::class.simpleName
    protected val activityViewModel: ActivityViewModel by viewModels()  // 기본 생성

    private var isFinish = false    // BackPress 두번 클릭 체크 값
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityBaseBinding>(this,
            R.layout.activity_base
        )

        binding.lifecycleOwner = this
        binding.activityViewModel = activityViewModel

        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_action_bar_exit)
        }
        supportFragmentManager.addOnBackStackChangedListener {
            var resId = R.drawable.ic_action_bar_exit
            if (supportFragmentManager.backStackEntryCount > 0) {
                resId = R.drawable.ic_action_bar_back
            }
            supportActionBar?.setHomeAsUpIndicator(resId)
        }
    }

    /**
     * 종료 체크 Toast
     */
    fun finishCheck(): Boolean {
        GlobalScope.launch {
            delay(3000)
            isFinish = false
        }
        if (!isFinish)
            Toast.makeText(this, getString(R.string.finish_toast), Toast.LENGTH_SHORT).show()
        return isFinish.apply { isFinish = true }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }
}