package com.example.android.unscramble.activity

import androidx.fragment.app.Fragment

interface IActivity {
    fun addFragment(fragment: Fragment)
}