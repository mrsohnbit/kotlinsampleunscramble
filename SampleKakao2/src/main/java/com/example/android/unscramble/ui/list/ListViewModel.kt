package com.example.android.unscramble.ui.list

import android.util.Log
import android.view.View
import androidx.lifecycle.*
import com.example.android.unscramble.DefaultDataSource
import com.example.android.unscramble.IDataSource
import com.example.android.unscramble.R
import com.example.android.unscramble.model.Documents
import com.example.android.unscramble.model.KakaoImage
import com.example.android.unscramble.model.ListLiveData
import com.example.android.unscramble.model.Meta
import com.example.android.unscramble.model.network.RequestImageParam
import com.example.android.unscramble.network.NetworkService
import com.example.android.unscramble.ui.BaseFragment
import kotlinx.coroutines.Dispatchers

class FragmentViewModel(private var fragment: BaseFragment, private val dataSource: IDataSource) : ViewModel() {
    private val TAG = this::class.simpleName
    private var receiveMeta = Meta(false, 1, 0)

    // 검색 결과 없음 표시
    private val _emptyView: MutableLiveData<Int> = MutableLiveData(View.GONE)
    val emptyView: LiveData<Int>
        get() = _emptyView

    private fun setMeta(isEnd: Boolean, pageableCount: Int, totalCount: Int) {
        receiveMeta = Meta(isEnd, pageableCount, totalCount)
    }

    private var paramSort: String = fragment.resources.getStringArray(R.array.sort_array_value)[0]
    private var paramPage: Int = 1
    // 검색 완료 했던 Text(스크롤시 EditText 값이 변하게 되면 잘못 된 데이터로 페이징 됨)
    private var beforeSearchText = ""

    private val _receiveKakaoImageData: MutableLiveData<KakaoImage> = MutableLiveData()
    val receiveKakaoImageData: LiveData<KakaoImage>
        get() = _receiveKakaoImageData

    val itemList = ListLiveData<Documents>()
    fun removeItem(item: Documents) {
        itemList.remove(item)
    }

    fun refreshFragment(fragment: BaseFragment) {
        this.fragment = fragment
    }

    /**
     * Spinner선택시 동작
     */
    fun onSpinnerSelected(position: Int) {
        paramSort = fragment.resources.getStringArray(R.array.sort_array_value)[position]
    }

    /**
     * 검색 버튼 클릭
     */
    fun onClickSearch(searchText: String) {
        if (searchText.isEmpty()) {
            dataSource.alertMessage("검색어를 입력 해주세요.")
            return
        }

        _emptyView.value = View.GONE
        beforeSearchText = searchText

        setMeta(false, 1, 0)
        itemList.clear(true)
        paramPage = 1
        netKakaoImage(searchText)
    }

    /**
     * RecyclerView 스크롤 페이징 처리
     */
    fun onLastScroll() {
        if (!receiveMeta.is_end) {
            netKakaoImage(beforeSearchText)
        }
    }

    /**
     * 검색 API 요청
     */
    private fun netKakaoImage(searchText: String) {
        dataSource.launchDataLoad(viewModelScope) {
            val param = RequestImageParam(searchText, paramPage, paramSort)
            val kakaoImage = NetworkService().allPlants(param)
            _receiveKakaoImageData.value = kakaoImage

            itemList.addAll(kakaoImage.documents)
            Log.i(TAG, "kakaoImage.documents.size=${kakaoImage.documents.size}")

            // 검색 데이터 없음 알림
            _emptyView.value = if(kakaoImage.documents.isEmpty()) View.VISIBLE else View.GONE

            receiveMeta = kakaoImage.meta
            paramPage += 1  // 검색 페이지 증가
        }
    }
}

/**
 * @activiyModel : Activity에서 사용되고 있는 ViewModel
 *                  progress, alert등 공통 영역을 담당 한다.
 */
class FragmentDataVMFactory(private val fragment: BaseFragment, private val activiyModel: ActivityViewModel) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return FragmentViewModel(fragment, DefaultDataSource(activiyModel, Dispatchers.IO)) as T
    }
}