/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.unscramble.activity

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.android.unscramble.R
import com.example.android.unscramble.databinding.ActivityMainBinding

class MainActivity: BaseActivity(), IActivity {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this,
            R.layout.activity_main)

        binding.lifecycleOwner = this
        binding.activityViewModel = activityViewModel

    }

    /**
     * Fragment 화면에 적용
     * 좌우 슬라이드 애니메이션 처리
     */
    override fun addFragment(fragment: Fragment) {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val transaction = supportFragmentManager.beginTransaction()
        val tag = fragment::class.simpleName
        transaction.setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit, R.anim.fragment_pop_enter, R.anim.fragment_pop_exit)
        transaction.replace(R.id.list_fragment, fragment, tag)
        transaction.addToBackStack(tag)
        try {
            transaction.commit()
        } catch (e: Exception) {
            e.printStackTrace()
            transaction.commitAllowingStateLoss()
        }


    }


    /**
     * 종료 처리
     * Fragment
     */
    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            super.onBackPressed()
        } else if (finishCheck()) {
            super.onBackPressed()
        }
    }
}