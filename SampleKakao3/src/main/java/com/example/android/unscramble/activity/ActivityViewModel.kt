package com.example.android.unscramble.ui.list

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ActivityViewModel: ViewModel() {
    private val TAG = this::class.simpleName

    private val _progressState = MutableLiveData(View.GONE)
    val progressState: LiveData<Int> = _progressState
    /**
     * Progress 상태 처리
     */
    fun progressStaus(showState: Int) {
            _progressState.value = showState
    }

    fun alertMessage(message: String) {
        _alertMessage.value = message
    }

    private val _alertMessage = MutableLiveData("")
    val alertMessage: LiveData<String> = _alertMessage


}