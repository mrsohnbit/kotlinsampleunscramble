package com.example.android.unscramble

import android.app.Application

class BaseApplication: Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: BaseApplication? = null
        fun applicationContext() : BaseApplication {
            return instance as BaseApplication
        }

        fun getResourceString(resId: Int): String {
            return applicationContext().resources.getString(resId)
        }

        fun getResourceStringArray(resId: Int): Array<out String> {
            return applicationContext().resources.getStringArray(resId)
        }
    }
}