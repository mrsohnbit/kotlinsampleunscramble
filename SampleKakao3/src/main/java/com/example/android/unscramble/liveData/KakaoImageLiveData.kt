package com.example.android.unscramble.liveData

import androidx.lifecycle.MutableLiveData
import com.example.android.unscramble.BaseApplication
import com.example.android.unscramble.R

class KakaoImageLiveData<T> : MutableLiveData<KakaoImage>() {

    private lateinit var mKakaoImage: KakaoImage
    val documentList = ListLiveData<Documents>()
    lateinit var metaData: Meta

    var paramPage = 1       // 페이징 번호
    var searchedText = ""   // 검색어

    var paramSort = BaseApplication.getResourceStringArray(R.array.sort_array_value)

    init {
        initBindValue()
    }

    fun initBindValue() {
        paramPage = 1
        setMeta(false, paramPage, 0)
        clear(true)
    }

    fun setImageData(kakaoImage: KakaoImage) {
        mKakaoImage = kakaoImage
        addAll(kakaoImage.documents)
        setMeta(kakaoImage.meta)

        paramPage += 1  // 검색 페이지 증가
    }

    fun add(item: Documents) {
        documentList.add(item)
    }

    fun addAll(items: List<Documents>) {
        documentList.addAll(items)
    }

    fun removeItem(items: Documents) {
        documentList.remove(items)
    }

    fun clear(notify: Boolean) {
        documentList.clear(notify)
    }

    fun setMeta(meta: Meta) {
        metaData = meta
    }

    fun setMeta(isEnd: Boolean, pageableCount: Int, totalCount: Int) {
        metaData = Meta(isEnd, pageableCount, totalCount)
    }
}