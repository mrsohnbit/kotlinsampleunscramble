package com.example.android.unscramble.liveData

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class KakaoImage(
    @SerializedName("documents")
    var documents: List<Documents>,
    @SerializedName("meta")
    var meta: Meta
)

data class Meta(
    @SerializedName("is_end")
    var is_end: Boolean,
    @SerializedName("pageable_count")
    var pageable_count: Int,
    @SerializedName("total_count")
    var total_count: Int
)

data class Documents(
    var idx: String,
    @SerializedName("collection")
    val collection: String,
    @SerializedName("datetime")
    val datetime: String,
    @SerializedName("display_sitename")
    var display_sitename: String,
    @SerializedName("doc_url")
    val doc_url: String,
    @SerializedName("height")
    var height: Int,
    @SerializedName("image_url")
    val image_url: String,
    @SerializedName("thumbnail_url")
    val thumbnail_url: String,
    @SerializedName("width")
    var width: Int
) : Serializable