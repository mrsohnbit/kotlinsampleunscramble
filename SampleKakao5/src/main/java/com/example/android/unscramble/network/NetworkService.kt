package com.example.android.unscramble.network

import com.example.android.unscramble.BuildConfig
import com.example.android.unscramble.liveData.Documents
import com.example.android.unscramble.liveData.KakaoImage
import com.example.android.unscramble.liveData.Plant
import com.example.android.unscramble.liveData.network.RequestImageParam
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.QueryMap
import java.io.IOException


interface KakaoService {

    @GET("v2/search/image")
    suspend fun getKakaoImage(@QueryMap param: Map<String, String>): KakaoImage

    @GET("googlecodelabs/kotlin-coroutines/master/advanced-coroutines-codelab/sunflower/src/main/assets/custom_plant_sort_order.json")
    suspend fun getCustomPlantSortOrder(): List<Documents>
}

interface SunflowerService {
    @GET("googlecodelabs/kotlin-coroutines/master/advanced-coroutines-codelab/sunflower/src/main/assets/plants.json")
    suspend fun getAllPlants(): List<Plant>

    @GET("googlecodelabs/kotlin-coroutines/master/advanced-coroutines-codelab/sunflower/src/main/assets/custom_plant_sort_order.json")
    suspend fun getCustomPlantSortOrder(): List<Plant>
}


class NetworkService() {

    val githubUrl = "https://raw.githubusercontent.com/"
    val kakaoUrl = "https://dapi.kakao.com/"

    private val retrofit = Retrofit.Builder()
        .baseUrl(kakaoUrl)
//        .client(OkHttpClient())
        .client(getOkHttpClient())
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val kakaoService = retrofit.create(KakaoService::class.java)
    suspend fun kakaoImageAPI(request: RequestImageParam): KakaoImage = withContext(Dispatchers.Default) {
        kakaoService.getKakaoImage(request.paramMap)
    }

//    suspend fun plantsByGrowZone(growZone: GrowZone) = withContext(Dispatchers.Default) {
//        val result = kakaoService.getAllPlants()
//        result.filter { it.growZoneNumber == growZone.number }.shuffled()
//    }

//    suspend fun customPlantSortOrder(): List<String> = withContext(Dispatchers.Default) {
//        val result = kakaoService.getCustomPlantSortOrder()
//        result.map { plant -> plant.plantId }
//    }

    /**
     * OKHttpClient 설정
     */
    fun getOkHttpClient(): OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE)
        if (BuildConfig.DEBUG) {
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        }

        class HeaderInterceptor: Interceptor {
            @Throws(IOException::class)
            override fun intercept(chain: Interceptor.Chain): Response {
                val newRequest = chain.request().newBuilder()
                    .addHeader("Authorization", "KakaoAK ca2b5da1cba84dde2a45f81cafbd84b0")
                    .build()
                return chain.proceed(newRequest)
            }
        }

        val okHttpBuilder = OkHttpClient.Builder()
//        okHttpBuilder.addInterceptor(headerInterceptor)
        okHttpBuilder.addInterceptor(HeaderInterceptor())
        okHttpBuilder.addInterceptor(httpLoggingInterceptor)

        return okHttpBuilder.build()
    }
}
