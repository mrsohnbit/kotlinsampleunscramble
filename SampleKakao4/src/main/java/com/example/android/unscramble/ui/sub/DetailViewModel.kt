package com.example.android.unscramble.ui.sub

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.android.unscramble.DefaultDataSource
import com.example.android.unscramble.IDataSource
import com.example.android.unscramble.activity.ActivityViewModel
import com.example.android.unscramble.network.receive.Documents
import kotlinx.coroutines.Dispatchers

class DetailViewModel(private val dataSource: IDataSource) : ViewModel() {
    private val TAG = this::class.simpleName

    private val _document = MutableLiveData<Documents>()
    val document: LiveData<Documents>
        get() = _document

    fun setDocument(documents: Documents) {
        _document.value = documents
    }
}

/**
 * @activiyModel : Activity에서 사용되고 있는 ViewModel
 *                  progress, alert등 공통 영역을 담당 한다.
 */
class DetailDataVMFactory(private val activiyModel: ActivityViewModel) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DetailViewModel(DefaultDataSource(activiyModel, Dispatchers.IO)) as T
    }
}