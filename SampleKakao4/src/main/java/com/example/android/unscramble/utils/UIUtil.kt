package com.example.android.unscramble.utils

import android.app.Activity
import android.content.Context
import android.view.inputmethod.InputMethodManager

object UIUtil {

    /**
     * 소프트웨어 키보드 내리기
     */
    fun keyboardHide(activity: Activity) {
        val view = activity.currentFocus
        if (view != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}