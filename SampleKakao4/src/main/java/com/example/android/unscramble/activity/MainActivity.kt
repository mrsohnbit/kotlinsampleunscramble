package com.example.android.unscramble.activity

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.android.unscramble.R
import com.example.android.unscramble.databinding.ActivityMainBinding
import com.example.android.unscramble.utils.UIUtil

class MainActivity: BaseActivity(), IActivity {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this,
            R.layout.activity_main)

        binding.lifecycleOwner = this
        binding.activityViewModel = activityViewModel

    }

    /**
     * Fragment 화면에 적용
     * 좌우 슬라이드 애니메이션 처리
     */
    override fun addFragment(fragment: Fragment) {
        UIUtil.keyboardHide(this)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val transaction = supportFragmentManager.beginTransaction()
        val tag = fragment::class.simpleName
        transaction.setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit, R.anim.fragment_pop_enter, R.anim.fragment_pop_exit)
        transaction.replace(R.id.list_fragment, fragment, tag)
        transaction.addToBackStack(tag)
        try {
            transaction.commit()
        } catch (e: Exception) {
            e.printStackTrace()
            transaction.commitAllowingStateLoss()
        }
    }


    /**
     * 종료 처리
     * Fragment
     */
    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            super.onBackPressed()
        } else if (finishCheck()) {
            super.onBackPressed()
        }
    }
}