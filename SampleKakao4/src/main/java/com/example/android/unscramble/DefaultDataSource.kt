package com.example.android.unscramble

import android.view.View
import com.example.android.unscramble.activity.ActivityViewModel
import com.example.android.unscramble.utils.NetworkUtil
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.net.UnknownHostException

/**
 * Activity와 Fragment에서 값을 공유 하며 ViewModel에서 Base역활을 한다.
 * Alert 메시지와 Progress등 공통적인 기능 처리 담당
 */
class DefaultDataSource(private val activityViewModel: ActivityViewModel, ioDispatcher: CoroutineDispatcher) : IDataSource {
    private val TAG = DefaultDataSource::class.simpleName
//    private val _showErrorToast = SingleLiveEvent<Void>()
//    val showErrorToast: LiveData<Void> = _showErrorToast
    /**
     * 네트워크 Thread 처리
     * 프로그래스 상태 처리 및 오류 알림을 위한 Try/Catch처리
     */
    override fun launchDataLoad(viewModelScope: CoroutineScope, block: suspend () -> Unit): Job {
        return viewModelScope.launch {
            try {
                if (!NetworkUtil.checkConnectNetwork(BaseApplication.applicationContext())) {
                    alertMessage(BaseApplication.getResourceString(R.string.connection_error_message))
                    return@launch
                }

                showProgress()
                block()
            } catch (error: Throwable) {
                error.printStackTrace()
                if (error is UnknownHostException) {
                    alertMessage("유효하지 않은 요청 입니다.")
                } else {
                    alertMessage("네트워크 상태를 확인해주세요. ${error.message}")
                }
                hideProgress()
                throw error
            } finally {
                hideProgress()
            }
        }
    }

    /**
     * 메시지 AlertDialog 띄우기
     */
    override fun alertMessage(message: String) {
        activityViewModel.alertMessage(message)
    }

    /**
     * Progress 상태 처리
     */
    override fun progressStaus(state: Int) {
        activityViewModel.progressStaus(state)
    }

    /**
     * Progress Layout을 visible 처리
     */
    override fun showProgress() {
        progressStaus(View.VISIBLE)
    }

    /**
     * Progress Layout을 gone 처리
     */
    override fun hideProgress() {
        progressStaus(View.GONE)
    }
}

interface IDataSource {
    fun progressStaus(state: Int)   // Progress 상태 저장
    fun showProgress()  // Progress Show
    fun hideProgress()  // Progress Hide
    // 네트워크 Exception 처리
    fun launchDataLoad(viewModelScope: CoroutineScope, block: suspend () -> Unit): Job
    // 메시지 다이얼로그
    fun alertMessage(message: String)
}
