package com.example.android.unscramble.ui.list

import android.util.Log
import android.view.View
import androidx.lifecycle.*
import com.example.android.unscramble.DefaultDataSource
import com.example.android.unscramble.IDataSource
import com.example.android.unscramble.activity.ActivityViewModel
import com.example.android.unscramble.liveData.KakaoImageLiveData
import com.example.android.unscramble.network.NetworkService
import com.example.android.unscramble.network.receive.KakaoImage
import com.example.android.unscramble.network.send.RequestImageParam
import kotlinx.coroutines.Dispatchers

class FragmentViewModel(private val dataSource: IDataSource) : ViewModel() {
    private val TAG = this::class.simpleName


    // 검색 결과 없음 표시
    private val _emptyView: MutableLiveData<Int> = MutableLiveData(View.GONE)
    val emptyView: LiveData<Int>
        get() = _emptyView

    val kakaoImageData = KakaoImageLiveData<KakaoImage>()
    private var paramSort: String = kakaoImageData.paramSort[0]

    /**
     * Spinner선택시 동작
     */
    fun onSpinnerSelected(position: Int) {
        paramSort = kakaoImageData.paramSort[position]
    }

    /**
     * 검색 버튼 클릭
     */
    fun onClickSearch(searchText: String) {
        if (searchText.isEmpty()) {
            dataSource.alertMessage("검색어를 입력 해주세요.")
            return
        }

        _emptyView.value = View.GONE
        kakaoImageData.initBindValue()
        kakaoImageData.searchedText = searchText
        netKakaoImage(searchText)
    }

    /**
     * RecyclerView 스크롤 페이징 처리
     */
    fun onLastScroll() {
        if (!kakaoImageData.metaData.is_end) {
            netKakaoImage(kakaoImageData.searchedText)
        }
    }

    /**
     * 검색 API 요청
     */
    private fun netKakaoImage(searchText: String) {
        dataSource.launchDataLoad(viewModelScope) {
            val param = RequestImageParam(searchText, kakaoImageData.paramPage, paramSort)
            val kakaoImage = NetworkService().kakaoImageAPI(param)

            kakaoImageData.setImageData(kakaoImage)
            Log.i(TAG, "kakaoImage.documents.size=${kakaoImage.documents.size}")

            // 검색 데이터 없음 알림
            _emptyView.value = if(kakaoImage.documents.isEmpty()) View.VISIBLE else View.GONE
        }
    }
}

/**
 * @activiyModel : Activity에서 사용되고 있는 ViewModel
 *                  progress, alert등 공통 영역을 담당 한다.
 */
class FragmentDataVMFactory(private val activiyModel: ActivityViewModel) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return FragmentViewModel(DefaultDataSource(activiyModel, Dispatchers.IO)) as T
    }
}