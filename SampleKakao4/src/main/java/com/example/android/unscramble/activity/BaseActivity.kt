package com.example.android.unscramble.activity

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.android.unscramble.R
import com.example.android.unscramble.databinding.ActivityBaseBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

open class BaseActivity : AppCompatActivity() {
    protected val TAG = this::class.simpleName
    protected val activityViewModel: ActivityViewModel by viewModels()  // 기본 생성

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityBaseBinding>(this,
            R.layout.activity_base
        )

        binding.lifecycleOwner = this
        binding.activityViewModel = activityViewModel

        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_action_bar_exit)
        }
        supportFragmentManager.addOnBackStackChangedListener {
            var resId = R.drawable.ic_action_bar_exit
            if (supportFragmentManager.backStackEntryCount > 0) {
                resId = R.drawable.ic_action_bar_back
            }
            supportActionBar?.setHomeAsUpIndicator(resId)
        }
    }

    /**
     * BackPress 종료 체크 Toast
     */
    private var isFinish = false    // BackPress 두번 클릭 체크 값
    fun finishCheck() {
        CoroutineScope(Dispatchers.Main).launch {
            delay(1500)
            isFinish = false
        }
        if (!isFinish)
            Toast.makeText(this, getString(R.string.finish_toast), Toast.LENGTH_SHORT).show()

        return if(isFinish) this.finishAffinity() else isFinish = true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }
}