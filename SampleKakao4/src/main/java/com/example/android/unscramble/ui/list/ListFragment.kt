package com.example.android.unscramble.ui.list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.*
import com.example.android.unscramble.R
import com.example.android.unscramble.databinding.FragmentListConstBinding
import com.example.android.unscramble.databinding.RecyclerviewItemBinding
import com.example.android.unscramble.network.receive.Documents
import com.example.android.unscramble.ui.BaseFragment
import com.example.android.unscramble.ui.sub.DetailFragment
import com.example.android.unscramble.utils.UIUtil

class ListFragment : BaseFragment() {
    private lateinit var binding: FragmentListConstBinding
    private val viewModel: FragmentViewModel by viewModels{ FragmentDataVMFactory(activityViewModel) }

    var beforeItemSize = 0  // 기존 Adapter의 Item갯수

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list_const, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        setListAdapter(binding.recyclerView).apply {
            bindObserve(this)
        }

//        coordinateMotion()
    }

//    private fun coordinateMotion() {
//        binding.appbarLayout.addOnOffsetChangedListener(
//            AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
//                val seekPosition = -verticalOffset / appBarLayout.totalScrollRange.toFloat()
//                binding.motionLayout.progress = seekPosition
//            }
//        )
//    }

    /**
     * FragmentViewModel의 itemList Observe
     * 데이터 변경시 RecyclerView ListAdapter 갱신
     * 주의 : Fragemnt에 넣어야 함(화면 회전시 이상 증상 발생)
     */
    private fun bindObserve(adapter: RecyclerListAdapter) {
        viewModel.kakaoImageData.documentList.observe(viewLifecycleOwner, Observer {
            it?.let {
                Log.i(TAG, "bindAdapter=${it.size}, beforeItemSize=${beforeItemSize}")
                UIUtil.keyboardHide(requireActivity())
                binding.searchEdittext.clearFocus()
                adapter.submitList(it)
                if (beforeItemSize < it.size) {
                    // 기존 데이터 갯수보다 많아질 경우 추가 된 위치만 Notify
                    adapter.notifyItemRangeInserted(beforeItemSize, it.size)
                    beforeItemSize = it.size
                } else {
                    if (beforeItemSize-1 > it.size) {
                        // 2개 이상의 데이터 변경 발생시 전체 갱신 처리
                        adapter.notifyDataSetChanged()
                    }
                    beforeItemSize = it.size
                }
            }
        })
    }

    /**
     * OnActivityResult 처리
     */
    val resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult(), ActivityResultCallback { data ->
        if (data.resultCode == AppCompatActivity.RESULT_OK) {
            Log.i(TAG, "data.resultCode RESULT_OK")
        } else {
            Log.i(TAG, "data.resultCode RESULT_CANCEL")
        }
    })


    /**
     * RecyclerView ListAdapter 적용
     */
    fun setListAdapter(recyclerView: RecyclerView): RecyclerListAdapter {
        val adapter = RecyclerListAdapter(this, viewModel)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
        recyclerView.addItemDecoration(DividerItemDecoration(recyclerView.context, DividerItemDecoration.VERTICAL))
        recyclerView.adapter = adapter
        return adapter
    }

    class RecyclerListAdapter(private var fragment: BaseFragment, private val mViewModel: FragmentViewModel): ListAdapter<Documents, RecyclerListAdapter.RecyclerViewHolder>(
        ListDiffCallBack()) {
        private val TAG = this::class.simpleName

        inner class RecyclerViewHolder(private val binding: RecyclerviewItemBinding) :
            RecyclerView.ViewHolder(binding.root) {
            fun bind(document: Documents) {
                document.idx = adapterPosition.toString()
                binding.document = document
                binding.deleteBtn.setOnClickListener {
                    binding.document?.let {
                        mViewModel.kakaoImageData.removeItem(it)

                        notifyItemRemoved(adapterPosition)
                    }
                }
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding: RecyclerviewItemBinding =
                DataBindingUtil.inflate(layoutInflater, R.layout.recyclerview_item, parent, false)
            return RecyclerViewHolder(binding)
        }

        override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
            holder.bind(getItem(position))
            holder.itemView.setOnClickListener {
                val detailFragment = DetailFragment()
                detailFragment.arguments = bundleOf("document" to getItem(position))
                fragment?.let { baseFragment ->
                    baseFragment.addFragment(detailFragment)
                }
            }
        }

        /**
         * ListAdapter의 데이터의 변경 부분만 갱신
         */
        class ListDiffCallBack: DiffUtil.ItemCallback<Documents>() {
            override fun areItemsTheSame(oldItem: Documents, newItem: Documents): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Documents, newItem: Documents): Boolean {
                return oldItem == newItem
            }
        }
    }
}
