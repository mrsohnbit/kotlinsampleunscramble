package com.example.android.unscramble.ui.game

import android.graphics.drawable.Drawable
import android.util.Log
import android.view.inputmethod.EditorInfo
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingListener
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.android.unscramble.R

/**
 * layout.xml의 ImageView에서 URL 입력하여 사용
 * 사용법 : imageFromUrl="@{modelView.imageUrl}"
 */
@BindingAdapter("imageFromUrl")
fun bindImageFromUrl(view: ImageView, imageUrl: String?) {
    if (!imageUrl.isNullOrEmpty()) {
        Glide.with(view.context)
                .load(imageUrl)
                .transition(DrawableTransitionOptions.withCrossFade())
//                .override(200,200)
                .error(ContextCompat.getDrawable(view.context, R.drawable.ic_error))
                .listener(object: RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean
                    ): Boolean {
                        Log.e("bindImageFromUrl", "onLoadFailed ${imageUrl}  ${e?.message}")
//                        view.setImageURI(Uri.parse(imageUrl))
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean
                    ): Boolean {
                        return false
                    }
                })
                .into(view)
    }
}


/**
 * RecyclerView Adapter의 마지막 스크롤
 * 사용법 : recyclerViewLastScroll="@{() -> viewModel.onScrollPaging()}"
 */
@BindingAdapter("recyclerViewLastScroll")
fun bindLastScroll(recyclerView: RecyclerView, listener: InverseBindingListener) {
    recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if (!recyclerView.canScrollVertically(1)) {
                listener.onChange()
            }
        }
    })
}


/**
 * 검색버튼 적용
 * 사용법 : imeActionSearch="@{() -> viewModel.onClickSearch(searchEdittext.getText().toString())}"
 * 자동 적용
 * android:imeOptions="actionSearch"
 * android:imeActionLabel="@string/text_search"
 * android:maxLines="1"
 * android:singleLine="true"
 */
@BindingAdapter("imeActionSearch")
fun bindImeActionSearch(textView: TextView, listener: InverseBindingListener) {
    textView.setImeActionLabel(textView.context.getString(R.string.text_search), EditorInfo.IME_ACTION_DONE)
    textView.imeOptions = EditorInfo.IME_ACTION_SEARCH
    textView.maxLines = 1
    textView.setSingleLine()
    textView.setOnEditorActionListener { tv, actionId, keyEvent ->
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            listener.onChange()
        }
        false
    }
}


/**
 * WebView에 ModelView의 Url값을 받아 url 바인딩
 * webViewUrl="@{detailViewModel.document.doc_url}"
 */
@BindingAdapter("webViewUrl")
fun webViewUrl(webView: WebView, url: String?) {
    if (!url.isNullOrEmpty()) {
        webView.settings.javaScriptEnabled = true
        webView.webViewClient = WebViewClient() // 새창 방지
        webView.loadUrl(url)
    }
}



//@BindingAdapter("isGone")
//fun bindIsGone(view: FloatingActionButton, isGone: Boolean?) {
//    if (isGone == null || isGone) {
//        view.hide()
//    } else {
//        view.show()
//    }
//}