package com.example.android.unscramble.network

import android.util.Log
import com.example.android.unscramble.BaseApplication
import com.example.android.unscramble.BuildConfig
import com.example.android.unscramble.R
import com.example.android.unscramble.network.receive.KakaoImage
import com.example.android.unscramble.network.send.RequestImageParam
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import com.google.gson.JsonSyntaxException
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.QueryMap


interface KakaoService {

    @GET("/v2/search/image")
    suspend fun getKakaoImage(@QueryMap param: Map<String, String>): KakaoImage
}

class NetworkService() {

    private val kakaoUrl = "https://dapi.kakao.com"

    private val retrofit = Retrofit.Builder()
        .baseUrl(kakaoUrl)
//        .client(OkHttpClient())
        .client(getOkHttpClient("KAKAO_API"))
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val kakaoService = retrofit.create(KakaoService::class.java)
    suspend fun kakaoImageAPI(request: RequestImageParam): KakaoImage {
        return kakaoService.getKakaoImage(request.paramMap)
    }

    /**
     * OKHttpClient 설정
     *  @param tag LogCat에 표시 될 TAG
     */
    private fun getOkHttpClient(tag: String): OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor(CustomHttpLogging(tag))
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE)
        if (BuildConfig.DEBUG) {
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        }

        class HeaderInterceptor: Interceptor {
            @Throws(Exception::class)
            override fun intercept(chain: Interceptor.Chain): Response {
                val newRequest = chain.request().newBuilder()
                    .addHeader("Authorization", "KakaoAK ${BaseApplication.getResourceString(R.string.kakao_api_key)}")
                    .build()
                return chain.proceed(newRequest)
            }
        }

        val okHttpBuilder = OkHttpClient.Builder()
        okHttpBuilder.addInterceptor(HeaderInterceptor())
        okHttpBuilder.addInterceptor(httpLoggingInterceptor)

        return okHttpBuilder.build()
    }

    /**
     * Json Pretty Print
     * @param tag LogCat에 표시 될 TAG
     */
    class CustomHttpLogging(val tag: String): HttpLoggingInterceptor.Logger {
        override fun log(message: String) {
            if (!message.startsWith("{")) {
                Log.d(tag, message)
                return
            }
            try {
                val gson = GsonBuilder().setPrettyPrinting().create()
                val jsonElement = JsonParser().parse(message)
                Log.d(tag, gson.toJson(jsonElement))
            } catch (m: JsonSyntaxException) {
                Log.d(tag, message)
            }
        }
    }
}
