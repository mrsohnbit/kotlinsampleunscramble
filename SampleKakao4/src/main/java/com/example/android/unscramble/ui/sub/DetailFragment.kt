/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package com.example.android.unscramble.ui.sub

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import com.example.android.unscramble.R
import com.example.android.unscramble.databinding.FragmentDetailBinding
import com.example.android.unscramble.network.receive.Documents
import com.example.android.unscramble.ui.BaseFragment

class DetailFragment: BaseFragment() {
    private lateinit var binding: FragmentDetailBinding

    private val viewModel: DetailViewModel by viewModels{ DetailDataVMFactory(activityViewModel) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.detailViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner


        arguments?.let {
            val document = it.getSerializable("document") as Documents
            Log.i(TAG, "document=${document}")
            viewModel.setDocument(document)

            binding.netButton.setOnClickListener {
//                CoroutineScope(Dispatchers.IO).launch {
//                }
//                val result = "subFragment Result"
//                setFragmentResult("requestKey", bundleOf("bundleKey" to result))

                document.display_sitename = ">>>>>>>이름 변경 됨<<<<<<<"
                setFragmentResult("document", bundleOf("document" to document))
                requireActivity().onBackPressed()
            }
        }

    }

    /**
     * OnActivityResult 처리
     */
    val resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult(), ActivityResultCallback { data ->
        if (data.resultCode == AppCompatActivity.RESULT_OK) {
            Log.i(TAG, "data.resultCode RESULT_OK")
        } else {
            Log.i(TAG, "data.resultCode RESULT_CANCEL")
        }
    })
}
