package com.example.android.unscramble.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.android.unscramble.R
import com.example.android.unscramble.databinding.ActivitySubBinding

class SubActivity: BaseActivity() {//: BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivitySubBinding>(this,
            R.layout.activity_sub
        )
        binding.lifecycleOwner = this
        binding.activityViewModel = activityViewModel
        setContentView(binding.root)
    }

    inline fun <reified T : Fragment> FragmentManager.showFragment(tag: String, container: Int, args: Bundle? = null) {
        val clazz = T::class.java
        var fragment = findFragmentByTag(clazz.name)
        if (fragment != null) {
            this.popBackStack(tag, 0)
        } else {
            fragment = (clazz.getConstructor().newInstance() as T).apply {
                args?.let {
                    it.classLoader = javaClass.classLoader
                    arguments = args
                }
            }
            val transaction = this.beginTransaction()
            transaction.add(container, fragment as Fragment, tag)
            transaction.addToBackStack(tag)
            transaction.commit()
        }
    }

    fun replaceFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        val tag = fragment::class.simpleName;
        transaction.replace(R.id.sub_fragment, fragment, tag)
        transaction.addToBackStack(tag)
        try {
            transaction.commit()
        } catch (e: Exception) {
            e.printStackTrace()
            transaction.commitAllowingStateLoss()
        }
    }


    companion object {
        val FRAGMENT_NAME = "FRAGMENT_NAME"
        fun startActivity(context: Context, fragment: Class<Fragment>, bundle: Bundle) {
            val intent = Intent(context, SubActivity::class.java)
            intent.putExtra(FRAGMENT_NAME, fragment.name)
            context.startActivity(intent)
        }
    }



}