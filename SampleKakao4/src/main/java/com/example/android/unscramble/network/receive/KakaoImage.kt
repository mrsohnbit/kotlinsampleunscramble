package com.example.android.unscramble.network.receive

import java.io.Serializable

data class KakaoImage(
    var documents: List<Documents>,
    var meta: Meta
)

data class Meta(
    var is_end: Boolean,
    var pageable_count: Int,
    var total_count: Int
)

data class Documents(
    var idx: String,
    val collection: String,
    val datetime: String,
    var display_sitename: String,
    val doc_url: String,
    var height: Int,
    val image_url: String,
    val thumbnail_url: String,
    var width: Int
) : Serializable