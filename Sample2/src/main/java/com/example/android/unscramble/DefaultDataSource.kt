package com.example.android.unscramble

import android.view.View
import com.example.android.unscramble.ui.list.ActivityViewModel
import kotlinx.coroutines.*
import java.net.UnknownHostException


/**
 * A source of data for [LiveDataViewModel], showcasing different LiveData + coroutines patterns.
 *
 * Activity와 Fragment에서 값을 공유 하며 ViewModel에서 Base역활을 한다.
 */
class DefaultDataSource(private val  activityViewModel: ActivityViewModel, private val ioDispatcher: CoroutineDispatcher) : IDataSource {
    private val TAG = DefaultDataSource::class.simpleName
//    private val _showErrorToast = SingleLiveEvent<Void>()
//    val showErrorToast: LiveData<Void> = _showErrorToast

    /**
     * 프로그래스 상태 처리 및 오류 알림을 위한 Try/Catch처리
     */
    override fun launchDataLoad(viewModelScope: CoroutineScope, block: suspend () -> Unit): Job {
        return viewModelScope.launch {
            try {
                showProgress()
                block()
            } catch (error: Throwable) {
                error.printStackTrace()
                if (error is UnknownHostException) {
                    alertMessage("유효하지 않은 요청 입니다.")
                } else {
                    alertMessage("네트워크 상태를 확인해주세요. ${error.message}")
                }
                hideProgress()
            } finally {
                hideProgress()
            }
        }
    }

    override fun alertMessage(message: String) {
        activityViewModel.alertMessage(message)
    }

    /**
     * Progress 상태 처리
     */
    override suspend fun progressStaus(showState: Int) {
        withContext(Dispatchers.Main) {
            activityViewModel.progressStaus(showState)
        }
    }

    /**
     * Progress Layout을 visible 처리
     */
    override suspend fun showProgress() {
        progressStaus(View.VISIBLE)
    }

    /**
     * Progress Layout을 gone 처리
     */
    override suspend fun hideProgress() {
        progressStaus(View.GONE)
    }
}

interface IDataSource {
    suspend fun progressStaus(state: Int)
    suspend fun showProgress()
    suspend fun hideProgress()
    fun launchDataLoad(viewModelScope: CoroutineScope, block: suspend () -> Unit): Job

    fun alertMessage(message: String)
}
