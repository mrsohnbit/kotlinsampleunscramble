package com.example.android.unscramble.ui.list

import android.content.ClipData
import android.util.Log
import androidx.lifecycle.*
import com.example.android.unscramble.DefaultDataSource
import com.example.android.unscramble.IDataSource
import com.example.android.unscramble.model.ListLiveData
import com.example.android.unscramble.model.Plant
import com.example.android.unscramble.network.NetworkService
import kotlinx.coroutines.Dispatchers

class FragmentViewModel(private val dataSource: IDataSource) : ViewModel() {
    private val TAG = this::class.simpleName

//    val itemList = ListLiveData<Plant>()
//
//    fun addItem(item: Plant) {
//        itemList.add(item)
//    }
//
//    fun addAll(items: List<Plant>) {
//        itemList.addAll(items)
//    }
//
//    fun removeItem(item: Plant) {
//        itemList.remove(item)
//    }




    val list = mutableListOf<Plant>()
    private val _itemList = MutableLiveData<List<Plant>>()
    val itemList: LiveData<List<Plant>> = _itemList

    fun addItem(item: Plant) {
        list.add(item)
        _itemList.value = list
    }

    fun addAll(plantList: List<Plant>) {
        list.addAll(plantList)
        _itemList.value = plantList
    }

    fun removeItem(item: Plant) {
        list.remove(item)
        _itemList.value = list
    }




//    private val tempList = mutableListOf<Plant>()
//    private val _plantList: MutableLiveData<List<Plant>> = MutableLiveData()
//    val plantList: LiveData<List<Plant>>
//        get() = _plantList
//
//
//    fun addAllItem(list: List<Plant>) {
//        tempList.addAll(list)
//        _plantList.value = list
//    }
//
//    fun removeItem(position: Int) {
//        tempList.removeAt(position)
//        _plantList.value = tempList
//    }
//
//    fun removeItem(plant: Plant) {
//        Log.i(TAG, "adapter.plantListSize1=${tempList.size}")
//
//        tempList.remove(plant)
//        Log.i(TAG, "adapter.plantListSize2=${tempList.size}")
//        _plantList.value = tempList
//    }


    fun testRetrofit() {
        dataSource.launchDataLoad(viewModelScope) {
            val list = NetworkService().allPlants()
            addAll(list)
        }
    }

    init {
//        _itemList.value = list
        testRetrofit()
    }

}

/**
 * @activiyModel : Activity에서 사용되고 있는 ViewModel
 *                  progress, alert등 공통 영역을 담당 한다.
 */
class FragmentDataVMFactory(private val activiyModel: ActivityViewModel) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return FragmentViewModel(DefaultDataSource(activiyModel, Dispatchers.IO)) as T
    }
}