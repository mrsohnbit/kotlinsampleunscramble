/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package com.example.android.unscramble.ui.list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.*
import com.example.android.unscramble.R
import com.example.android.unscramble.databinding.ListFragmentBinding
import com.example.android.unscramble.databinding.RecyclerviewItemBinding
import com.example.android.unscramble.model.Plant
import com.example.android.unscramble.ui.game.MAX_NO_OF_WORDS

/**
 * Fragment where the game is played, contains the game logic.
 */
class ListFragment : Fragment() {
    private val TAG = this::class.simpleName

    private lateinit var binding: ListFragmentBinding

    private val activityViewModel: ActivityViewModel by activityViewModels()
    private val viewModel: FragmentViewModel by viewModels{ FragmentDataVMFactory(activityViewModel) }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.list_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.listViewModel = viewModel
        binding.maxNoOfWords = MAX_NO_OF_WORDS
        binding.lifecycleOwner = viewLifecycleOwner

        binding.netButton.setOnClickListener {
            viewModel.testRetrofit()
        }

        val adapter = RecyclerListAdapter()
        adapter.setViewModel(viewModel)
        binding.recylerView.adapter = adapter
        binding.recylerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recylerView.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
        subscribeUi(adapter)

    }


    private fun subscribeUi(adapter: RecyclerListAdapter) {
        viewModel.itemList.observe(viewLifecycleOwner) { itemList ->
            Log.i(TAG, "adapter.adapterCount=${adapter.itemCount}")
            Log.i(TAG, "adapter.plantListSize=${itemList.size}")


            adapter.submitList(itemList, {
                Log.i(TAG, "after.plantListSize=${adapter.itemCount}")
                adapter.notifyDataSetChanged()
            })
//            adapter.notifyDataSetChanged()+
        }
    }


    class RecyclerListAdapter: ListAdapter<Plant, RecyclerListAdapter.ViewHolder>(ListDiffCallBack()) {
        private val TAG = this::class.simpleName

        lateinit var mViewModel: FragmentViewModel
        fun setViewModel(viewModel: FragmentViewModel) {
            mViewModel = viewModel
        }


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//            return ViewHolder.create(parent)
            val holder = ViewHolder(RecyclerviewItemBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false))
            return holder
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val current = getItem(position)
            current.index = position
            holder.bind(current)
            holder.deleteBtn.setOnClickListener{
                Log.i(TAG, "position=${position}")

                mViewModel?.let {
                    it.removeItem(current)
                }
            }
        }

        class ViewHolder(private val binding: RecyclerviewItemBinding) : RecyclerView.ViewHolder(binding.root) {
            val deleteBtn: Button = itemView.findViewById(R.id.deleteBtn)
            val indexTv: TextView = itemView.findViewById(R.id.index_textview)
            fun bind(item: Plant?) {
                binding.apply {
                    plant = item
                    executePendingBindings()
                }

//            private val wordItemView: TextView = itemView.findViewById(R.id.textView)
//            private val deleteBtn: Button = itemView.findViewById(R.id.deleteBtn)
//                item let {
////                    val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault()).format(Date(it.updateDate))
//                    wordItemView.text = dto.name
//                    deleteBtn.setOnClickListener{
//                    }
//                }
            }
        }


        /**
         * ListAdapter의 데이터의 변경 부분만 갱신
         */
        class ListDiffCallBack: DiffUtil.ItemCallback<Plant>() {
            override fun areItemsTheSame(oldItem: Plant, newItem: Plant): Boolean {
                val isChange = oldItem.plantId == newItem.plantId
                return isChange
            }

            override fun areContentsTheSame(oldItem: Plant, newItem: Plant): Boolean {
                val isChange = oldItem == newItem
                return isChange
            }
        }
    }
}
