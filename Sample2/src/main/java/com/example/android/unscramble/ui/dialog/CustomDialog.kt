package com.example.android.unscramble.ui.dialog

import android.app.AlertDialog
import android.os.Bundle
import com.example.android.unscramble.R
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import androidx.annotation.Nullable

class CustomDialog(context: Context?) : AlertDialog(context!!) {
    private val TAG = this::class.simpleName

    private lateinit var mMessage: String

    private lateinit var mMessageView: TextView
    private lateinit var mNoButton: Button
    private lateinit var mOkButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val layoutParam = WindowManager.LayoutParams()
        layoutParam.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND
        layoutParam.dimAmount = 0.8f
        val view = LayoutInflater.from(context).inflate(R.layout.popup_dialog, null, false)
        view.layoutParams = layoutParam
        setContentView(view)

        mMessageView = findViewById(R.id.dialog_title_textview)
        mNoButton = findViewById(R.id.cancel_btn)
        mOkButton = findViewById(R.id.confirm_btn)

        setMessage(mMessage)
        mOkButton.setOnClickListener{
            dismiss()
        }
    }

    fun setMessage(message: String): CustomDialog {
        mMessage = message
        if (this::mMessageView.isInitialized) {
            mMessageView.text = mMessage
        }
        return this
    }

    fun setConfirmListener(clickListener: View.OnClickListener): CustomDialog {
        if (this::mOkButton.isInitialized) {
            mOkButton.setOnClickListener(View.OnClickListener {
                clickListener.onClick(it)
                dismiss()
            })
        }
        return this
    }


    fun setCancelListener(clickListener: View.OnClickListener): CustomDialog {
        if (this::mNoButton.isInitialized) {
            mNoButton.setOnClickListener(View.OnClickListener {
                clickListener.onClick(it)
                dismiss()
            })
        }
        return this
    }


    companion object {
        fun show(@Nullable context: Context): CustomDialog {
            val dialog = CustomDialog(context)
            dialog.show()
            return dialog
        }

        fun show(@Nullable context: Context, message: String): CustomDialog {
            val dialog = CustomDialog(context)
            dialog.setMessage(message)
            dialog.show()
            return dialog
        }
    }

    //    companion object {
//        lateinit var yesClick: (Boolean) -> Unit
//        lateinit var message: String
//        fun getInstance(context: Context, message: String, yesClick: (Boolean) -> Unit): CustomDialog {
//            this.yesClick = yesClick
//            this.message = message
//            return CustomDialog(context)
//        }
//    }

}