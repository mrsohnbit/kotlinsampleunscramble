/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.unscramble.activity

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.android.unscramble.R
import com.example.android.unscramble.databinding.MainActivityBinding
import com.example.android.unscramble.ui.dialog.CustomDialog
import com.example.android.unscramble.ui.list.ActivityViewModel

class MainActivity : AppCompatActivity() {
    private val TAG = this::class.simpleName

    private val activityViewModel: ActivityViewModel by viewModels()  // 기본 생성

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<MainActivityBinding>(this,
            R.layout.main_activity
        )
        binding.lifecycleOwner = this
        binding.activityViewModel = activityViewModel

        // 알림 메시지
        activityViewModel.alertMessage.observe(this) {message ->
            if (!message.isEmpty()) {
                CustomDialog.show(this, message)
            }
        }

//        CustomDialog.show(this, "hihi").setConfirmListener(View.OnClickListener {
//            Toast.makeText(this, "toast", Toast.LENGTH_SHORT).show()
//        })

    }
}