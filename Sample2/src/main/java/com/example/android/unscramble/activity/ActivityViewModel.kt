package com.example.android.unscramble.ui.list

import android.view.View
import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ActivityViewModel: ViewModel() {
    private val TAG = this::class.simpleName

    private val _progressState = MutableLiveData(View.GONE)
    val progressState: LiveData<Int> = _progressState
    /**
     * Progress 상태 처리
     */
    suspend fun progressStaus(showState: Int) {
        withContext(Dispatchers.Main) {
            _progressState.value = showState
        }
    }

    fun alertMessage(message: String) {
        _alertMessage.value = message
    }
    private val _alertMessage = MutableLiveData("")
    val alertMessage: LiveData<String> = _alertMessage


}